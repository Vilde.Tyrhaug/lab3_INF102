package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

  @Override
  public int peakElement(List<Integer> numbers) {
    return peakHelper(numbers, 0, 0);
  }

  public int peakHelper(List<Integer> numbers, int index, int prevElement) {
    if (index == numbers.size()) {
      return 0;
    }

    int currentElement = numbers.get(index);
    int nextElement = peakHelper(numbers, index + 1, currentElement);

    if (isPeak(prevElement, currentElement, nextElement)) return currentElement;

    return nextElement;
  }

  private boolean isPeak(int prev, int current, int next) {
    return current >= prev && current >= next;
  }
}
