package INF102.lab3.numberGuesser;

public class MyGeniusGuesser implements IGuesser {

  private int lowerbound;
  private int upperbound;

  @Override
  public int findNumber(RandomNumber number) {
    lowerbound = number.getLowerbound();
    upperbound = number.getUpperbound();

    int numberGuess = -1;

    while (lowerbound <= upperbound) {
      int mid = lowerbound + (upperbound - lowerbound) / 2;
      int guessResult = number.guess(mid);

      if (guessResult == 0) {
        numberGuess = mid; // Update numberGuess when the guess is correct
        break; // Exit the loop because we found the correct number
      } else if (guessResult == 1) {
        upperbound = mid - 1;
      } else {
        lowerbound = mid + 1;
      }
    }
    return numberGuess;
  }
}
