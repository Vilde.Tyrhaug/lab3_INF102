package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

  @Override
  public long sum(List<Long> list) {
    return sumHelper(list, 0);
  }

  private long sumHelper(List<Long> list, int index) {
    if (index == list.size()) {
      return 0; // Base case: return 0 when the index is equal to the list size.
    }

    long currentElement = list.get(index); // Get the value of current element.
    long sumOfRest = sumHelper(list, index + 1); // Recursively call sumHelper with the next index.

    return currentElement + sumOfRest; // Return the sum of the current element and the sum of the rest of the list.
  }
}
